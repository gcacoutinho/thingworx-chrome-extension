// Called when the user clicks on the browser action.
chrome.browserAction.onClicked.addListener((tab) => {
    // No tabs or host permissions needed!
    if (tab.url.includes("/Thingworx/Composer")) {
        chrome.tabs.executeScript({
            file: 'thirdParty/jquery-3.2.1.slim.min.js'
        });
        chrome.tabs.executeScript({
            file: 'work.js'
        });
    }
});