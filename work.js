// Main modal
var mainModal = $('body > div.ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.ui-dialog-buttons.ui-draggable');
mainModal.css('width', '90vw');
mainModal.css('left', '5vw');
mainModal.css('height', '95vh');
mainModal.css('top', '10px');

// Size fix
$('#configure-bindings-dialog').css('height', '75%');
$('#grid-config-list-ul').css('height', '70vh');
$('#mashup-parameter-table').css('height', '70vh');

// Scroll fix
$('#configure-bindings-dialog').css('overflow-y', 'auto');
$("#event-binding-data-services").css('overflow-y', 'auto');
$('#configure-bindings-dialog-tabs').css('overflow-y', 'auto');
$('#event-binding-widget-services > div').css('overflow-y', 'auto');
$('#configure-bindings-dialog-tabs').css('overflow-x', 'auto');
//$('#event-binding-widget-services').css('overflow-x', 'scroll');