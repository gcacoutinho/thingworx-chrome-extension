### Thingworx fixer | Chrome extension
***
This extension is intended to correct some of the Thingworx's ~~bugs~~ features.  
For now the **only** way to activate this extension is by **clicking** its **icon**.  
##### Instalation
***
1. Navigate to chrome://extensions
2. Expand the Developer dropdown menu and click “Load Unpacked Extension”
3. Navigate to the local folder containing the extension’s code and click Ok
4. Assuming there are no errors, the extension should load into your browser